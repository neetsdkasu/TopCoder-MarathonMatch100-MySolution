#include <iostream>
#include <vector>
#include <string>
#include "solution.cpp"

using namespace std;

int main() {
    int H;
    cin >> H;
    vector<string> board(H);
    for (int i = 0; i < H; i++) {
        cin >> board[i];
    }
    SameColorPairs scp;
    vector<string> ret = scp.removePairs(board);
    cout << ret.size() << endl;
    for (auto it = ret.begin(); it != ret.end(); it++) {
        cout << *it << endl;
    }
    cout.flush();
}