Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

	Public Sub Main()
		Try
			Dim N As Integer = CInt(Console.ReadLine())
			Dim data(N - 1) As String
			For i As Integer = 0 To UBound(data)
				data(i) = Console.ReadLine()
			Next i
			
			Dim scp As New SameColorPairs()
			Dim ret() As String = scp.removePairs(data)

			Console.WriteLine(ret.Length)
			For Each r As String In ret
				Console.WriteLine(r)
			Next r
			Console.Out.Flush()
			
		Catch Ex As Exception
			Console.Error.WriteLine(ex)
		End Try
	End Sub

End Module