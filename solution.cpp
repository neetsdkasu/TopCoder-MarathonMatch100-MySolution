#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <cmath>
#include <random>

#ifdef _MSC_VER
#include <chrono>
inline auto get_time() {
	return std::chrono::system_clock::now();
}
inline auto to_msec(int t) {
	return std::chrono::milliseconds(t);
}
using Time = std::chrono::system_clock::time_point;
#else
const double ticks_per_sec = 2800000000;
inline double get_time() {
	uint32_t lo, hi;
	asm volatile ("rdtsc" : "=a" (lo), "=d" (hi));
	return (((uint64_t)hi << 32) | lo) / ticks_per_sec;
}
inline double to_msec(int t) {
	return double(t) / 1000.0;
}
using Time = double;
#endif

using namespace std;

using Tp = pair<int, int>;
using Table = vector<vector<int> >;

const int NoColor = 0xFFFF;

class SameColorPairs {
    size_t H, W;
    int Colors;
    size_t index1[1024], index2[1024], index3[1024];
    Table table;
    mt19937 gen;
    vector<Tp> cps;
    int rand(int x) { return gen() % x; }
public:
    SameColorPairs() : Colors(0), gen(19831983) {}
    vector<string> removePairs(vector<string> board) {
        auto time0 = get_time() + to_msec(9800);
        H = board.size();
        W = board[0].length();
        table.resize(H);
        for (size_t row = 0; row < H; row++) {
            for (size_t col = 0; col < W; col++) {
                int color = (int)board[row][col] - '0';
                table[row].push_back(1 << color);
                Colors = max(Colors, color + 1);
                cps.push_back({row * row + col * col, (row << 8) | col});
            }
        }
        sort(cps.begin(), cps.end());
        for (int i = 0; i < Colors; i++) {
            index1[1 << i] = i;
            index2[1 << i] = i + Colors;
            index3[1 << i] = i + Colors + Colors;
        }
        vector<size_t> res;
        
        auto time1 = get_time();
        auto time2 = time1;
        auto diff = time1 - time2;
        int cnt = 0;
        for (;;) {
            time2 = time1;
            time1 = get_time();
            auto tmp_diff = time1 - time2;
            if (tmp_diff > diff) { diff = tmp_diff; }
            if (time1 + diff > time0) { break; }
            cnt++;
            auto tmp = approach0();
            if (tmp.size() > res.size()) {
                swap(tmp, res);
                if (res.size() * 2 == H * W) {
                    cerr << "early exit!" << endl;
                    break;
                }
            }
        }
        
        vector<string> ret;
        for (auto it = res.begin(); it != res.end(); it++) {
            auto e = *it;
            auto row1 = e & 0xFF;
            auto col1 = (e >> 8) & 0xFF;
            auto row2 = (e >> 16) & 0xFF;
            auto col2 = (e >> 24) & 0xFF;
            ret.push_back(
                to_string(row1) + " " +
                to_string(col1) + " " +
                to_string(row2) + " " +
                to_string(col2)
            );
        }
        
        cerr << "try " << cnt << endl;
        
        return ret;
    }

private:
    
    vector<size_t> approach0() {

        vector<size_t> ret;
        Table tbx(H);
        for (size_t row = 0; row < H; row++) {
            tbx[row].assign(table[row].begin(), table[row].end());
        }
        vector<size_t> ps(max(H, W));
        for (size_t i = 0; i < H * W * 30; i++) {
            bool face = rand(100) < 50;
            size_t row1, col1, row2, col2;
            if (face) {
                row1 = rand(H - 1);
                col1 = rand(W);
            } else {
                row1 = rand(H);
                col1 = rand(W - 1);
            }
            int color1 = tbx[row1][col1];
            if (color1 == NoColor) { continue; }
            ps.clear();
            if (face) {
                for (size_t row = row1 + 1; row < H; row++) {
                    int color = tbx[row][col1];
                    if (color == NoColor) { continue; }
                    if (color != color1) { break; }
                    ps.push_back(row);
                }
                if (ps.size() == 0) { continue; }
                row2 = ps[rand(ps.size())];
                col2 = col1;
            } else {
                for (size_t col = col1 + 1; col < W; col++) {
                    int color = tbx[row1][col];
                    if (color == NoColor) { continue; }
                    if (color != color1) { break; }
                    ps.push_back(col);
                }
                if (ps.size() == 0) { continue; }
                row2 = row1;
                col2 = ps[rand(ps.size())];
            }
            tbx[row1][col1] = NoColor;
            tbx[row2][col2] = NoColor;
            ret.push_back(
                row1
                | (col1 << 8)
                | (row2 << 16)
                | (col2 << 24)
            );
        }
        if (ret.size() * 2 == H * W) {
            return ret;
        }
        vector<vector<Tp> > brss(Colors * 3);
        for (size_t row = 0; row < H; row++) {
            for (size_t col = 0; col < W; col++) {
                int e = tbx[row][col];
                if (e == NoColor) { continue; }
                brss[index1[e]].push_back({row, col});
            }
        }
        for (size_t col = 0; col < W; col++) {
            for (size_t row = 0; row < H; row++) {
                int e = tbx[row][col];
                if (e == NoColor) { continue; }
                brss[index2[e]].push_back({row, col});
            }
        }
        for (auto it = cps.begin(); it != cps.end(); it++) {
            auto z = it->second;
            auto row = z >> 8;
            auto col = z & 0xFF;
            int e = tbx[row][col];
            if (e == NoColor) { continue; }
            brss[index3[e]].push_back({row, col});
        }
        for (auto it = brss.begin(); it != brss.end(); ) {
            if (it->size() == 0) {
                it = brss.erase(it);
            } else {
                it++;
            }
        }
        
        vector<int> ret2;
        
        for (int zzz = 0; zzz < 5; zzz++) {
            vector<int> tmp;
            vector<vector<Tp> > rss(brss.size());
            for (size_t i = 0; i < brss.size(); i++) {
                rss[i].assign(brss[i].begin(), brss[i].end());
            }
            Table tb(H);
            for (size_t row = 0; row < H; row++) {
                tb[row].assign(tbx[row].begin(), tbx[row].end());
            }
            
            size_t ri = 0;
            size_t d = max(H, W) * 3 / 2;
            for (size_t i = 0; i < H * W * 40; i++) {
                ri++;
                if (ri >= rss.size()) { ri = 0; }
                auto rs = rss.begin() + ri;
                if (rs->size() == 0) {
                    rss.erase(rs);
                    if (rss.size() == 0) {
                        ret.insert(ret.end(), tmp.begin(), tmp.end());
                        return ret;
                    }
                    continue;
                }
                size_t x = rand(rs->size());
                auto xp = rs->begin() + x;
                auto e = tb[xp->first][xp->second];
                if (e == NoColor) {
                    rs->erase(xp);
                    if (rs->size() == 0) {
                        rss.erase(rs);
                        if (rss.size() == 0) {
                            ret.insert(ret.end(), tmp.begin(), tmp.end());
                            return ret;
                        }
                    }
                    continue;
                }
                int c = 0;
                auto end = min(x + 1 + d, rs->size());
                for (size_t y = x + 1; y < end; y++) {
                    auto yp = rs->begin() + y;
                    if (tb[yp->first][yp->second] == NoColor) { continue; }
                    auto row1 = min(xp->first, yp->first);
                    auto row2 = xp->first + yp->first - row1;
                    auto col1 = min(xp->second, yp->second);
                    auto col2 = xp->second + yp->second - col1;
                    for (auto row = row1; row <= row2; row++) {
                        for (auto col = col1; col <= col2; col++) {
                            if ((tb[row][col] & e) == 0) {
                                goto OutOfLoop;
                            }
                        }
                    }
                    tb[xp->first][xp->second] = NoColor;
                    tb[yp->first][yp->second] = NoColor;
                    tmp.push_back(
                        xp->first
                        | (xp->second << 8)
                        | (yp->first << 16)
                        | (yp->second << 24)
                    );
                    rs->erase(yp);
                    rs->erase(rs->begin() + x);
                    if (rs->size() == 0) {
                        rss.erase(rs);
                        if (rss.size() == 0) {
                            ret.insert(ret.end(), tmp.begin(), tmp.end());
                            return ret;
                        }
                    }
                    break;
                OutOfLoop:
                    c++;
                    if (c > 4) { break; }
                }
            }
            
            if (tmp.size() > ret2.size()) {
                swap(tmp, ret2);
            }
            
        }

        ret.insert(ret.end(), ret2.begin(), ret2.end());
        return ret;
    }
    
};