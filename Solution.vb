Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports Tp = System.Tuple(Of Integer, Integer)

Public Class SameColorPairs
	Dim rand As New Random(19831983)
	Dim H As Integer
    Dim W As Integer
    Dim Board(,) As Integer
    Dim Colors As Integer = 0
    Dim Index1(1024) As Integer
    Dim Index2(1024) As Integer
    Const NoColor As Integer = &HFFFF
    
	Public Function removePairs(board() As String) As String()
		Dim time0 As Integer = Environment.TickCount + 9400
		Console.Error.WriteLine("{0}, {1}", time0, rand.Next(0, 100))
		
        H = board.Length
        W = board(0).Length
        ReDim Me.Board(H - 1, W - 1)
        For row As Integer = 0 To H - 1
            ' Console.Error.WriteLine(board(row))
            For col As Integer = 0 To W - 1
                Dim color As Integer = Asc(board(row)(col)) - Asc("0")
                Me.Board(row, col) = 1 << color
                Colors = Math.Max(Colors, color + 1)
            Next col
        Next row
        For i As Integer = 0 To Colors - 1
            Index1(1 << i) = i
            Index2(1 << i) = i + Colors
        Next i
        
		Dim ret As New List(Of String)()
        
        Dim time1 As Integer = Environment.TickCount
        Dim time2 As Integer = time1
        Dim diff As Integer = 0
        Dim cnt As Integer = 0
        Do
            time2 = time1
            time1 = Environment.TickCount
            diff = Math.Max(diff, time1 - time1)
            If time1 + diff > time0 Then Exit Do
            cnt += 1
            Dim tmp As List(Of String) = Approach0()
            If tmp.Count > ret.Count Then ret = tmp
        Loop
        
        Console.Error.WriteLine("try {0}", cnt)
        
		removePairs = ret.ToArray()
		
	End Function
    
    Private Function Approach0() As List(Of String)
        Dim ret As New List(Of String)()
        Dim tb(H - 1, W - 1) As Integer
        For row As Integer = 0 To H - 1
            For col As Integer = 0 To W - 1
                tb(row, col) = Board(row, col)
            Next col
        Next row
        Dim ps As New List(Of Integer)()
        For i As Integer = 0 To H * W * 50
            Dim face As Boolean = rand.Next(100) < 50
            Dim row1 As Integer, col1 As Integer
            Dim row2 As Integer, col2 As Integer
            If face Then
                row1 = rand.Next(H - 1)
                col1 = rand.Next(W)
            Else
                row1 = rand.Next(H)
                col1 = rand.Next(W - 1)
            End If
            Dim color1 As Integer = tb(row1, col1)
            If color1 = NoColor Then Continue For
            ps.Clear()
            If face Then
                For row As Integer = row1 + 1 To H - 1
                    Dim color As Integer = tb(row, col1)
                    If color = NoColor Then Continue For
                    If color <> color1 Then Exit For
                    ps.Add(row)
                Next row
                If ps.Count = 0 Then Continue For
                row2 = ps(rand.Next(ps.Count))
                col2 = col1
            Else
                For col As Integer = col1 + 1 To W - 1
                    Dim color As Integer = tb(row1, col)
                    If color = NoColor Then Continue For
                    If color <> color1 Then Exit For
                    ps.Add(col)
                Next col
                If ps.Count = 0 Then Continue For
                row2 = row1
                col2 = ps(rand.Next(ps.Count))
            End If
            ret.Add(String.Format("{0} {1} {2} {3}", _
                row1, col1, row2, col2))
            tb(row1, col1) = NoColor
            tb(row2, col2) = NoColor
        Next i
        If ret.Count * 2 = H * W Then
            Approach0 = ret
            Exit Function
        End If
        Dim rss As New List(Of List(Of Tp))()
        For i As Integer = 0 To Colors - 1
            rss.Add(New List(Of Tp)())
            rss.Add(New List(Of Tp)())
        Next i
        For row As Integer = 0 To H - 1
            For col As Integer = 0 To W - 1
                Dim e As Integer = tb(row, col)
                If e = NoColor Then Continue For
                rss(Index1(e)).Add(New Tp(row, col))
            Next col
        Next row
        For col As Integer = 0 To W - 1
            For row As Integer = 0 To H - 1
                Dim e As Integer = tb(row, col)
                If e = NoColor Then Continue For
                rss(Index2(e)).Add(New Tp(row, col))
            Next row
        Next col
        Dim ri As Integer = 0
        Dim d As Integer = Math.Max(H, W) * 3 \ 2
        For i As Integer = 0 To H * W * 50
            ri += 1
            If ri >= rss.Count Then ri = 0
            Dim rs As List(Of Tp) = rss(ri)
            If rs.Count = 0 Then
                rss.RemoveAt(ri)
                If rss.Count = 0 Then
                    Approach0 = ret
                    Exit Function
                End If
                Continue For
            End If
            Dim x As Integer = rand.Next(rs.Count)
            Dim xp As Tp = rs(x)
            Dim e As Integer = tb(xp.Item1, xp.Item2)
            If e = NoColor Then
                rs.RemoveAt(x)
                If rs.Count = 0 Then
                    rss.RemoveAt(ri)
                    If rss.Count = 0 Then
                        Approach0 = ret
                        Exit Function
                    End If
                End If
                Continue For
            End If
            Dim c As Integer = 0
            For y As Integer = x + 1 To Math.Min(x + 1 + d, rs.Count - 1)
                Dim yp As Tp = rs(y)
                If tb(yp.Item1, yp.Item2) = NoColor Then Continue For
                Dim row1 As Integer = Math.Min(xp.Item1, yp.Item1)
                Dim row2 As Integer = xp.Item1 + yp.Item1 - row1
                Dim col1 As Integer = Math.Min(xp.Item2, yp.Item2)
                Dim col2 As Integer = xp.Item2 + yp.Item2 - col1
                For row As Integer = row1 To row2
                    For col As Integer = col1 To col2
                        If (tb(row, col) And e) = 0 Then
                            GoTo OutOfLoop
                        End If
                    Next col
                Next row
                tb(xp.Item1, xp.Item2) = NoColor
                tb(yp.Item1, yp.Item2) = NoColor
                ret.Add(String.Format("{0} {1} {2} {3}", _
                    xp.Item1, xp.Item2, yp.Item1, yp.Item2))
                rs.RemoveAt(y)
                rs.RemoveAt(x)
                If rs.Count = 0 Then
                    rss.RemoveAt(ri)
                    If rss.Count = 0 Then
                        Approach0 = ret
                        Exit Function
                    End If
                End If
                Exit For
            OutOfLoop:
                c += 1
                If c > 4 Then Exit For
            Next y
        Next i
        Approach0 = ret
    End Function
End Class